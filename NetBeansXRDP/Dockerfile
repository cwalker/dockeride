FROM fedora:22

RUN dnf -y groupinstall \
    'Xfce Desktop' \
    && yum clean all

# http://sigkillit.com/2013/02/26/how-to-remotely-access-linux-from-windows/
COPY Xclients /etc/skel/.Xclients

RUN dnf -y install \
    supervisor \
    xrdp \
    && dnf clean all

RUN useradd johan && \
    echo johan:secret | chpasswd

# Supervisord should run xrdp
COPY xrdp.ini /etc/supervisord.d/

# Allow all users to connect
RUN sed -i '/TerminalServerUsers/d' /etc/xrdp/sesman.ini && \
    sed -i '/TerminalServerAdmins/d' /etc/xrdp/sesman.ini

# Install Java
RUN dnf -y install java java-devel
ENV JAVA_HOME /usr/lib/jvm/java-openjdk

WORKDIR /tmp

# Install NetBeans
RUN wget http://download.netbeans.org/netbeans/8.0.2/final/bundles/netbeans-8.0.2-linux.sh
RUN chmod +x netbeans*.sh
RUN sh netbeans*.sh --silent

# Install Eclipse
RUN wget http://ftp.fau.de/eclipse/technology/epp/downloads/release/mars/R/eclipse-jee-mars-R-linux-gtk-x86_64.tar.gz
RUN tar -xzvf eclipse*.tar.gz -C /opt

# Install IntelliJ
RUN wget http://download.jetbrains.com/idea/ideaIC-14.1.5.tar.gz
RUN mkdir -p /opt/intellij
RUN tar -xf idea*.tar.gz --strip-components=1 -C /opt/intellij

# Define folders for Netbeans
RUN sed -i 's/^netbeans_default_userdir.*/netbeans_default_userdir=\/workspace\/userdir/' /usr/local/netbeans-8.0.2/etc/netbeans.conf
RUN sed -i 's/^netbeans_default_cachedir.*/netbeans_default_cachedir=\/workspace\/cachedir/' /usr/local/netbeans-8.0.2/etc/netbeans.conf

# Cleanup
RUN rm -rf /tmp/*

EXPOSE 3389
CMD ["supervisord", "-n"]
