docker run -d \
	   -v $PWD/workspace:/workspace \
           -v $PWD/m2:/root/.m2 \
	   -p 3389:3389 \
	   -p 8083:8080 \
	   netbeansxrdp
