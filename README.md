# README #

Some examples to run Eclipse, IntelliJ and Netbeans within a Docker containter. The examples show how you can connect to the IDE using VNC, RDP or X11.

Beware these are just examples that can serve for inspiration. I didn't completely verify if everything is working correctly. I only use these Docker containers for presentations, not for my daily job. For my daily job we have similar Docker containers, but I unfortunately cannot share them.