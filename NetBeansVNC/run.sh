docker run -d \
	   -v $PWD/workspace:/workspace \
           -v $PWD/m2:/root/.m2 \
	   -p 5900:5900 \
           -p 8082:8080 \
           netbeansvnc
